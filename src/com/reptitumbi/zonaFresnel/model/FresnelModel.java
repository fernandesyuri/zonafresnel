package com.reptitumbi.zonaFresnel.model;

/**
 *
 * @author Yuri, Waislan, William e Eduardo
 */
public interface FresnelModel {
    
    // Retorna a altura da torre Tx em metros
    public double getAlturaTorreTx();
    
    // Retorna a altura da torre Rx em metros
    public double getAlturaTorreRx();
    
    // Retorna a potência do transmissor em watts
    public double getPotenciaTransmissor();
    
    // Retorna a atenuação do cabo em dB/100m
    public double getAtenuacaoCabo();
    
    // Retorna a atenuação do conector em dB
    public double getAtenuacaoConector();
    
    // Retorna o ganho da antena Tx em dBi
    public double getGanhoAntenaTx();
    
    // Retorna o ganho da antena Rx em dBi
    public double getGanhoAntenaRx();
    
    // Retorna a distância do radio enlace em km
    public double getDistanciaEnlace();
    
    // Retorna a frequência de operação em MHz
    public double getFrequenciaOperacao();
    
    // Retorna o tipo de atenuação do cabo (db/m ou db/100m)
    public int getTipoAtenuacaoCabo();
}
