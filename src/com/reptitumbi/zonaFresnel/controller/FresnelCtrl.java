package com.reptitumbi.zonaFresnel.controller;

import com.reptitumbi.zonaFresnel.view.TelaResultados;
import com.reptitumbi.zonaFresnel.model.FresnelModel;

/**
 *
 * @author Yuri, Waislan, William e Eduardo
 */
public class FresnelCtrl {

    private FresnelModel telaPrincipal;

    public FresnelCtrl(FresnelModel telaPrincipal) {

        this.telaPrincipal = telaPrincipal;
    }

    public void calcula() {

        double atenuacaoCabo = 0;
        switch (telaPrincipal.getTipoAtenuacaoCabo()) {
            case 0:
                atenuacaoCabo = telaPrincipal.getAtenuacaoCabo();
                break;
            case 1:
                atenuacaoCabo = telaPrincipal.getAtenuacaoCabo() / 100;
                break;
        }

        // Peirp = Px - (2 * AConector) - (alturaTx * ACabo / 100) + GTx
        double px = 10 * Math.log10(telaPrincipal.getPotenciaTransmissor() / 0.001);
        System.out.println(px);
        double peirp = px - (2 * telaPrincipal.getAtenuacaoConector()) - (telaPrincipal.getAlturaTorreTx() * atenuacaoCabo) + telaPrincipal.getGanhoAntenaTx();

        // Ae = 32,44 + 20log(d[km]) + 20log(f[MHz])
        double ae = 32.44 + (20 * Math.log10(telaPrincipal.getDistanciaEnlace())) + (20 * Math.log10(telaPrincipal.getFrequenciaOperacao()));
        System.out.println("Ae: " + ae);

        // Pr = Peirp - Ae - (2 * Aconector) - (alturaRx * ACabo / 100)
        double pr = peirp - ae - (2 * telaPrincipal.getAtenuacaoConector()) - (telaPrincipal.getAlturaTorreRx() * atenuacaoCabo);

        // raioZonaFresnel[m] = 17,32 * raiz(d[km] / f[GHz])
        // double raioZonaFresnel = 17.32 * Math.sqrt(telaPrincipal.getDistanciaEnlace() / (telaPrincipal.getFrequenciaOperacao() / 1000));
        double raioZonaFresnel = 550 * Math.sqrt((Math.pow(telaPrincipal.getDistanciaEnlace(), 2) / 4) / (telaPrincipal.getDistanciaEnlace() * telaPrincipal.getFrequenciaOperacao()));

        double[] raiosZonaFresnel = new double[101];
        for (int i = 0; i < 101; i++) {
            double d2 = telaPrincipal.getDistanciaEnlace() - ((double) i / 100) * telaPrincipal.getDistanciaEnlace();
            double d1 = telaPrincipal.getDistanciaEnlace() - d2;

//          if(telaPrincipal.getAlturaTorreTx() == telaPrincipal.getAlturaTorreRx())
            raiosZonaFresnel[i] = 550 * Math.sqrt((d1 * d2) / (telaPrincipal.getDistanciaEnlace() * telaPrincipal.getFrequenciaOperacao()));
            System.out.println(raiosZonaFresnel[i]);
        }

        TelaResultados resultados = new TelaResultados(telaPrincipal, pr, raioZonaFresnel, peirp, raiosZonaFresnel);
        resultados.setVisible(true);
    }
}
